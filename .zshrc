
#Start tmux
# if [ "$TMUX" = "" ]; then tmux; fi

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.config/zsh/oh-my-zsh

# ---- From the Backup ----
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000000
SAVEHIST=1000000
setopt autocd extendedglob
#bindkey -v
zstyle :compinstall filename '/home/v/.zshrc'

autoload -Uz compinit
compinit
#
# PS1='%F{blue}%~ %(?.%F{green}.%F{red})%#%f '
#

# Function to get the Git status
function git_prompt_info() {
    # Return Git branch/status information if in a Git repository
    local git_info=$(git rev-parse --is-inside-work-tree 2>/dev/null)
    if [[ $git_info == "true" ]]; then
        echo "%F{white}$(git branch --show-current 2>/dev/null)%f"
    fi
}

# Function to change the prompt based on Vim mode
function zle-keymap-select {
    if [[ ${KEYMAP} == vicmd ]]; then
        # Normal Mode (Red color for the last part)
        PROMPT="%B%F{white}[$(git_prompt_info)] %F{blue}%~ %F{red}%#%f%b "
    else
        # Insert Mode (Green color for the last part)
        PROMPT="%B%F{white}[$(git_prompt_info)] %F{blue}%~ %F{green}%#%f%b "
    fi
    zle reset-prompt
}

zle-line-init() {
    zle -K viins  # Start in Insert mode
}

zle -N zle-keymap-select
zle -N zle-line-init

# Initial prompt setup
PROMPT="%B%F{white}[git:$(git_prompt_info)] %F{blue}%~ %F{green}%#%f%b "



# variables to make RANGER open files with the correct editor
export VISUAL=nvim
export EDITOR=nvim

# ---- End from the Backup ----

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
# Themese makes you laggy
# ZSH_THEME="eastwood"


# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
export ZSH_CUSTOM=$HOME/.config/zsh/custom/

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=( 
    git 
    zsh-vi-mode
    zsh-autosuggestions
)

source $ZSH/oh-my-zsh.sh
bindkey '^y' autosuggest-accept

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
 if [[ -n $SSH_CONNECTION ]]; then
   export EDITOR='vim'
 else
   export EDITOR='nvim'
 fi

# Sourcing additional files
# Lauterbach export
# source $ZSH_CUSTOM/lauterbach.sh
# Current project aliases.h
# source $ZSH_CUSTOM/aliases_work.zsh

# >>> juliaup initialize >>>

# !! Contents within this block are managed by juliaup !!

path=('/home/v/.juliaup/bin' $path)
export PATH

# <<< juliaup initialize <<<

PATH="/home/v/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/v/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/v/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/v/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/v/perl5"; export PERL_MM_OPT;
